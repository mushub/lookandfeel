

<pre><code>Function :: BckUp >>></code></pre>

#### look and feel ::GTK:: Musgreen theme and Dutch_green_next
simple gtk theme and icons :: made with oomox :: small :: I3wm

## MUSGREEN
![ArcoLinux_2020-08-04_16-40-25](https://user-images.githubusercontent.com/50716324/89307414-58578280-d671-11ea-93ed-265b2c9cba21.png)
 
 ##### Extract as user or system-wide
 ###### Musgreen and Dutch-Green-next
 ~/.themes or /usr/share/themes
 
 ###### Orville Icons
 ~/.icons or /usr/share/icons
 
 ---


## DUTCH_GREEN_NEXT 
###### (icon :: evolvere-ubuntu)

![Schermafdruk_2020-08-30_11-45-03](https://user-images.githubusercontent.com/50716324/91656128-78366680-eab6-11ea-9fdd-9ea0726e4b3f.png)

--------------------

#### xfce4-weather icons

###### location WSky-Light-green $ /usr/share/xfce4/weather/icons
--------------------
![Schermafdruk_2020-09-15_00-54-03](https://user-images.githubusercontent.com/50716324/93145990-06812e00-f6ee-11ea-97ac-3b4d9a48d954.png)


